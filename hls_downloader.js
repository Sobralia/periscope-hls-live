var fs = require('fs');
var request = require('request');

module.exports = function(params){
    var _this = this;
    this._construct = function(params){

    this.retry = 5;
    this.retry_left = 5;
    this.sequence = -1;
    this.chunks = [];

    this.url = params.url;
    this.path = params.path;
    this.output = params.output;
    this.chunk_base_url = params.url.substring(0, params.url.indexOf("playlist"));
    this.tmp_path = params.path + "/tmp/";
  };

  this.download_m3u8 = function(){
    request(this.url, function(err, response, body){
      if (response.statusCode === 404 && _this.retry_left > 0){
        _this.retry_left--;
        console.log(_this.retry_left + " left");
      }else if (response.statusCode === 200){
        var raw_data = body.split("\n");

        _this.retry_left = _this.retry;
        if (raw_data[0] === "#EXTM3U"){
          if (raw_data[3].indexOf("#EXT-X-MEDIA-SEQUENCE") > -1){
            _this.parse_m3u8(raw_data);
          }
        }
      }
      if (_this.retry_left == 0){
        console.log("no more retry");
        _this.stop();
      }else{
        setTimeout(function(){
          _this.download_m3u8();
        }, 2000);
      }
    });
  };

  this.parse_m3u8 = function(raw_data){
    var i = 0;
    var length = raw_data.length;

    while (i < length){
      //Only lines that contains a chunk file
      if (raw_data[i].indexOf(".ts") > -1){
        _this.download_chunks(raw_data[i]);
      }
      i++;
    }
  };

  this.download_chunks = function(chunk_name){
    var filepath = _this.tmp_path + chunk_name;
    var url = _this.chunk_base_url + chunk_name;

    //Check if chunk was already downloaded
    if (_this.chunks.indexOf(chunk_name) == -1){
      var dest = fs.createWriteStream(filepath);
      _this.chunks.push(chunk_name);

      request.get(url)
      .on("error", function(err){
        console.log(err);
      })
      .pipe(dest);
      console.log("Downloaded: " + chunk_name);
    }
  };

  this.start = function(){
    this.download_m3u8();
  };

  this.create_final_file = function(){
    var i = 0;
    var length = _this.chunks.length;
    var file = fs.WriteStream(_this.output);
    var data;

    while (i < length){
      data = fs.createReadStream(_this.tmp_path + _this.chunks[i]);
      data.pipe(file);
      data.close();
      i++;
    }

    file.end();
  };

  this.stop = function(){
    _this.create_final_file();
  };

  return (this._construct(params));
};
