# periscope-hls-live

### Version
0.0.1

Download ts chunk and concatenate them from a periscope's live

### Installation
```sh
git clone https://framagit.org/Sobralia/periscope-hls-live.git
```

### Usage
```javascript
var Downloader = require('./hls_downloader.js');
var params = {
  url: "URL_OF_M3U8",
  path: "PATH/TO/SAVE/FOLDER",
  output: "PATH/TO/FILE.ts"
}

var liveRecorder = new Downloader(params);
liveRecorder.start(params);
```

# Changelog
